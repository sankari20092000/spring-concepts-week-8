package com.hcl.springrest.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.springrest.entity.Employee;

@RestController
@RequestMapping("/v1/employee")
public class EmployeeRestController {

		//@RequestMapping(value="/add",method = RequestMethod.GET)
		
		@PostMapping("/add")
		public String  add(@RequestBody Employee emp ) {
			
			System.out.println(emp);
			
			return "Employee Added successfully";
				
			
		}
		
		@GetMapping("/get")
		public  String  get() {
			
			
			return "show employee details";
			
			
		}
		
		@PutMapping("/update")
		public String  update() {
			
			return "employee updated";
			
		}
		
		@DeleteMapping("/remove")
		public String  remove() {
			
			return "employee removed/deleted";
			
		}
		
		
		
		
	
	
}
