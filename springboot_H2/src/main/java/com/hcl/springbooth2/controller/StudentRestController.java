package com.hcl.springbooth2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.springbooth2.entity.Student;
import com.hcl.springbooth2.service.IStudentService;

@RestController
@RequestMapping("student/v1")
public class StudentRestController {

	
		@Autowired
		IStudentService service;
	
	@PostMapping(value = "/add",consumes = "application/json",produces = "application/json")
	public  Student  insert(@RequestBody  Student student) {
		
		return service.addStudent(student);
		
		
		
	}
	
	@GetMapping(value="/getall",produces = "application/json")
	public List<Student>  getAll(){
		
		return  service.getAllStudent();
		
	}
	
	@PutMapping(value="/update",consumes = "application/json")
	public Student  update(@RequestBody Student student) {
		
		return service.updateStudent(student);
		
	}
	
	
	
}
