package com.hcl.client;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import com.hcl.beans.Employee;

public class Main {

	public static void main(String[] args) {


				//step 1 read spring container reads xml file 
		
		//BeanFactory factory = new XmlBeanFactory(new ClassPathResource("application.xml"));
		
		
		ApplicationContext  context = new ClassPathXmlApplicationContext("application.xml");
		
				//Employee emp =(Employee)		factory.getBean("emp");
						
					Employee emp =	context.getBean("emp2",Employee.class);
				
				System.out.println(emp.getEid() +" "+emp.getEname()+" "+emp.getSalary());
		
				Employee emp3 =	context.getBean("emp3",Employee.class);
				
				System.out.println(emp3.getEid() +" "+emp3.getEname()+" "+emp3.getSalary());
				
				
				
	}

}
