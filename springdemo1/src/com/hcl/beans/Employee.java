package com.hcl.beans;

public class Employee {

	private int eid;
	private String ename;
	private double salary;

	public Employee() {
		
		System.out.println("Employee() is called object created..");

	}

	public Employee(int eid, String ename, double salary) {
		super();
		this.eid = eid;
		this.ename = ename;
		this.salary = salary;
		
		System.out.println("param Employee constructor is called object created..");
	}

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		
		System.out.println("setEid() is called...");
		this.eid = eid;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

}
