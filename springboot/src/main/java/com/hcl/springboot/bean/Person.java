package com.hcl.springboot.bean;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Person {

	private int pid;
	private String pname;
	public void setPid(int i) {
		this.pid=pid;
	}
	public void setPname(String string) {
		this.pname=pname;
	}
	public int getPid() {
		return pid;
	}
	public String getPname() {
		return pname;
	}
	
	
	
}
