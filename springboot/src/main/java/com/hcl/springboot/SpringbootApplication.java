package com.hcl.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.hcl.springboot.bean.Person;

@SpringBootApplication
public class SpringbootApplication {

	public static void main(String[] args) {
	 ApplicationContext context =	SpringApplication.run(SpringbootApplication.class, args);

	 
	 Person person =		context.getBean(Person.class);
	
	 	System.out.println(person);
	 	
	 	person.setPid(101);
	 	person.setPname("tom");
	 	
	 	System.out.println(person.getPid() +" "+person.getPname());
	 
	
	}

}
