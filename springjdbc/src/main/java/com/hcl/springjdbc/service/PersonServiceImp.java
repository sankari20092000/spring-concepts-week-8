package com.hcl.springjdbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.springjdbc.beans.Person;
import com.hcl.springjdbc.dao.IPersonDAO;
import com.hcl.springjdbc.dao.PersonDaoImp;

@Service("service")
public class PersonServiceImp implements IPersonService{

	
	@Autowired
	IPersonDAO dao; // new PersonDaoImp(); at run time by @Repository
	
	
	@Override
	public void addPerson(Person person) {

			dao.addPerson(person);
			
		
	}

	@Override
	public void updatePerson(Person person) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void selectById(int personId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteById(int personId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Person> selectAllPersons() {
		
		return	dao.selectAllPersons();
			
	}

}
