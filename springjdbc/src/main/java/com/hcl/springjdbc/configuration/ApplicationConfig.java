package com.hcl.springjdbc.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@PropertySource(value = {"classpath:application.properties"})
public class ApplicationConfig {
	
	@Autowired
	private Environment env;
	
	@Bean("ds")
	public DataSource getDataSource() {
		
			
		
			System.out.println(env.getRequiredProperty("jdbc.driver"));
		
			System.out.println(env.getRequiredProperty("username"));
			
			
			DriverManagerDataSource dataSource = new DriverManagerDataSource();
			
			dataSource.setDriverClassName(env.getRequiredProperty("jdbc.driver"));
			dataSource.setUrl(env.getRequiredProperty("jdbc.url"));
			dataSource.setUsername(env.getProperty("jdbc.username"));
			dataSource.setPassword(env.getRequiredProperty("jdbc.password"));
			
		
		return  dataSource;
			
	}
	
	
		@Bean
		public JdbcTemplate  jdbcTemplate(DataSource ds) {
			
			
			JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);
			
				
				jdbcTemplate.setResultsMapCaseInsensitive(true);
				
				return jdbcTemplate;
			
			
			
		}
	
	

}
