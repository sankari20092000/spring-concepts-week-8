package com.hcl.springjdbc;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.hcl.springjdbc.beans.Person;
import com.hcl.springjdbc.service.IPersonService;

/**
 * Hello world!
 *
 */
@ComponentScan(basePackages = "com.hcl.springjdbc")
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Welcome to Spring JDBC App" );
        
        
        ApplicationContext context = new AnnotationConfigApplicationContext(App.class);

   	IPersonService service =(IPersonService)context.getBean("service");
        
   			Person person  =context.getBean(Person.class);
   	
			/*
			 * person.setPersonId(114); person.setName("smith"); person.setAge(25);
			 * 
			 * System.out.println(person);
			 * 
			 * 
			 * service.addPerson(person);
			 */
   				
   			
   			
   		List<Person>	 list =	service.selectAllPersons();
   				
   				
   				list.stream().forEach((p1)->{System.out.println(p1.getName()+" "+p1.getAge());});
   			
   	
   	
		/*
		 * JdbcTemplate jdbcTemplate = context.getBean(JdbcTemplate.class);
		 * 
		 * System.out.println(jdbcTemplate);
		 */
        	
        		
        	
        	
        	
        
    }
}
