package com.hcl.springjdbc.dao;

import java.util.List;

import com.hcl.springjdbc.beans.Person;

public interface IPersonDAO {
	
	

	public void  addPerson(Person person);
	
	public void  updatePerson(Person person);
	
	public void  selectById(int personId);
	
	public void deleteById(int personId);
	
	public    List<Person>    selectAllPersons();

}
