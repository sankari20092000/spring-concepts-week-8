package com.hcl.springjdbc.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hcl.springjdbc.beans.Person;

@Repository
public class PersonDaoImp implements IPersonDAO {

	
	
	@Autowired
	JdbcTemplate  jdbcTemplate;
	
	@Override
	public void addPerson(Person person) {


  int count =   jdbcTemplate.update("insert into person(pid,name,age) values(?,?,?)",person.getPersonId(),person.getName(),person.getAge());
		
  			if(count > 0) {
  				
  				
  				System.out.println(count+" record inserted..");
  				
  			}

	}

	@Override
	public void updatePerson(Person person) {
		// TODO Auto-generated method stub

	}

	@Override
	public void selectById(int personId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteById(int personId) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Person> selectAllPersons() {

		List<Person> personsList =		jdbcTemplate.query("select * from person",new BeanPropertyRowMapper(Person.class));
		
		return personsList;
	}

}
